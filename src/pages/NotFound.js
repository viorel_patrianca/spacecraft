import React from 'react';
import Alert from 'react-bootstrap/Alert';
import Container from 'react-bootstrap/Container';

const NotFound = () => {
    return (
        <Container className="p-1">
            <div className="col-sm-4">
                <Alert variant="danger mx-auto mt-4">
                    <Alert.Heading>Opps!</Alert.Heading>
                    <hr/>
                    <p className="mb-0">
                        Page not found!s
                    </p>
                </Alert>
            </div>
        </Container>
    )
}

export default NotFound;
