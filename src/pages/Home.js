import React, {useEffect, Fragment, useState} from "react";
import {connect} from 'react-redux';
import moment from 'moment';
import ReactLoading from 'react-loading';
import {Form} from 'react-bootstrap';
import logo from '../../src/assets/spacex-logo.png';
import rocket from '../../src/assets/img/launch-home.png'
import spinner from '../../src/assets/icon/refresh.png'
import asc from '../../src/assets/icon/sort.png'
import './Home.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {getIntroAction, getOrdCallAction, getYearAction} from "../actions";


const Home = (props) => {
    const {getIntroAction} = props;
    const [check, setCheck] = useState(false);

    const [type, setType] = useState("Filter by Year");


    useEffect(() => {
        getIntroAction();
    }, [getIntroAction]);

    return (
        <div className="container mt-2">
            <nav className="navbar navbar-light bg-white ">
                <img className="app-logo" src={logo} alt="logo"/> <span className="logo-title">LAUNCHES</span>
                <div className="left-circle"></div>
                <div className="reload">
                    <span>Reload Data</span>
                    <button onClick={() => {
                        props.getIntroAction()
                    }}><img className="" src={spinner} alt="spinner"/></button>
                </div>
            </nav>

            <div className="row vh-100 header-margin">
                <div className="col-md-5 py-3">
                    <div className="card sticky-top border-0 rocket">
                        <img src={rocket} alt="Card"/>
                    </div>
                </div>
                <div className="col-md-7 py-3 ">
                    <div className="row">
                        <div className="col-md-6"></div>
                        <div className="col-md-6">
                            <div className="row p-lg-0 ">
                                <div className="col-md-6">

                                    <Form.Select
                                        value={type}
                                        onChange={e => {
                                            props.getYearAction(e.target.value);
                                            setType(e.target.value);
                                        }}
                                    >
                                        <option>Filter by Year</option>
                                        <option>2006</option>
                                        <option>2007</option>
                                        <option>2008</option>
                                        <option>2009</option>
                                        <option>2010</option>
                                        <option>2011</option>
                                        <option>2012</option>
                                        <option>2013</option>
                                        <option>2014</option>
                                        <option>2015</option>
                                        <option>2016</option>
                                        <option>2017</option>
                                        <option>2018</option>
                                        <option>2019</option>
                                        <option>2020</option>
                                        <option>2021</option>
                                    </Form.Select>

                                </div>
                                <div className="col-md-6">
                                    <div className="filter">
                                        <span>Sort Descending</span>
                                        <button
                                            onClick={() => {
                                                setCheck(prevCheck => !prevCheck);
                                                props.getOrdCallAction(check)
                                            }}

                                        ><img className="" src={asc} alt="asc"/></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col-md-3"></div>
                        <div className="col-md-9 ">

                            {props.message &&
                            <div className="alert alert-warning" role="alert">
                                No entry for year {props.message}!
                            </div>
                            }

                            {props.loading &&
                            <div className="d-flex justify-content-center mt-5">
                                <ReactLoading
                                    type='bars'
                                    color='#282733'
                                    height={'15%'}
                                    width={'15%'}
                                />
                            </div>
                            }

                            {Object.keys(props.data).length !== 0 &&
                            <Fragment>
                                {props.data.map((item, key) => {
                                    return (
                                        <div className="button-list" key={key}>
                                            <div className="row">
                                                <div className="col-xs-2 col-sm-2 m-0 p-0">
                                                    <div className="index-nr">#{item.flight_number}</div>
                                                </div>
                                                <div className="col-xs-5 col-sm-6  m-0 p-0">
                                                    <div className="ship">{item.mission_name}</div>
                                                </div>
                                                <div className="col-xs-5 col-sm-4  m-0 p-0">
                                                    <div className="m-0 p-0">
                                                        <div
                                                            className="date-ship">{moment(item.launch_date_local).format('Do MMM YYYY')}</div>
                                                    </div>
                                                    <div className="m-0 p-0">
                                                        <div className="ship-two">{item.rocket.rocket_name}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })
                                }
                            </Fragment>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>

    );
}

const mapStateToProps = state => {
    return {
        data: state.spacecraft.data,
        loading: state.spacecraft.loading,
        message: state.spacecraft.message
    };
};

export default connect(mapStateToProps, {getIntroAction, getOrdCallAction, getYearAction})(Home);