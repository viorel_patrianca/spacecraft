import React from 'react';
import {Route, Switch} from 'react-router-dom';
import {connect} from "react-redux";
import Home from "../pages/Home";
import NotFound from "../pages/NotFound";



const AppContainer = (props) => {

    return (
        <div>
            <Switch>
                <Route exact path='/' component={Home}/>
                <Route component={NotFound}/>


            </Switch>

        </div>
    )
}

export default connect()(AppContainer);
