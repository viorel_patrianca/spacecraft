import axios from 'axios';


export const getDefault = () => {
    return axios.get('https://api.spacexdata.com/v3/launches');
};


export const getOrderApi = (data) => {
    return axios.get('https://api.spacexdata.com/v3/launches?order='+data);
};

export const getYearApi = (data) => {
    return axios.get('https://api.spacexdata.com/v3/launches?launch_year='+data);
};