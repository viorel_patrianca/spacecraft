import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
import storage from 'redux-persist/lib/storage';
import rootReducer from '../reducers';
import rootSaga from '../sagas';


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const sagaMiddleware = createSagaMiddleware();

const persistConfig = {
  key: 'root',
  whitelist:['userToken'],
  storage,
};


const reducer = persistReducer(persistConfig, rootReducer);
export const store = createStore(reducer, {}, composeEnhancers(applyMiddleware(sagaMiddleware)));
export const persistor = persistStore(store);
sagaMiddleware.run(rootSaga);
