import {all} from 'redux-saga/effects';
import spacecraftSagas from './spacecraftSagas'

export default function* rootSaga() {
    yield all([
        ...spacecraftSagas
    ]);
}