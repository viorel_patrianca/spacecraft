import {takeLatest, call, fork, put, delay} from 'redux-saga/effects';
import * as actions from '../actions/spacecraftAction';
import * as api from '../api/spacecraft';


function* getDataIntro(action) {
    try {
        yield put(actions.getMessageAction(''));
        yield put(actions.getDataAction({}))
        const response = yield call(api.getDefault);
        yield put(actions.getLoadingAction(true));
        yield delay(300);

        if (response.data.length > 0) {
            yield put(actions.getLoadingAction(false));
            yield put(actions.getDataAction(response.data))
        }

    } catch (e) {
        console.log(e);
        yield put(actions.getLoadingAction(false));
    }
}

function* getOrd(action) {
    try {
        yield put(actions.getMessageAction(''));
        let data = action.id === true ? 'ASC' : 'DESC';
        yield put(actions.getDataAction({}))
        const response = yield call(api.getOrderApi, data);
        yield put(actions.getLoadingAction(true));
        yield delay(300);

        if (response.data.length > 0) {
            yield put(actions.getLoadingAction(false));
            yield put(actions.getDataAction(response.data))
        }

    } catch (e) {
        console.log(e);
        yield put(actions.getLoadingAction(false));
    }
}

function* getYearData(action) {
    try {

        let data = action.id;

        yield put(actions.getMessageAction(''));
        yield put(actions.getDataAction({}))

        if (!isNaN(data)) {

            const response = yield call(api.getYearApi, data);
            yield put(actions.getLoadingAction(true));
            yield delay(300);

            if (response.data.length > 0) {
                yield put(actions.getLoadingAction(false));
                yield put(actions.getDataAction(response.data))
            } else {
                yield put(actions.getMessageAction(data));
                yield put(actions.getLoadingAction(false));
            }
        } else {
            yield put(actions.getMessageAction(''));
            yield put(actions.getDataAction({}))
            const response = yield call(api.getDefault);
            yield put(actions.getLoadingAction(true));
            yield delay(300);

            if (response.data.length > 0) {
                yield put(actions.getLoadingAction(false));
                yield put(actions.getDataAction(response.data))
            }
        }

    } catch (e) {
        console.log(e);
        yield put(actions.getLoadingAction(false));
    }
}

function* watchKeyRequest() {
    yield takeLatest(actions.Types.GET_INTRO, getDataIntro);
}

function* watchYearRequest() {
    yield takeLatest(actions.Types.YEAR_DATA, getYearData);
}

function* watchOrdRequest() {
    yield takeLatest(actions.Types.ORD_CALL, getOrd);

}

const spacecraftSagas = [
    fork(watchKeyRequest),
    fork(watchOrdRequest),
    fork(watchYearRequest)
];

export default spacecraftSagas;