import React from 'react';
import AppContainer from './navigation/AppNavigator';
import history from "./navigation/history";
import {Provider} from 'react-redux';
import {persistor, store} from './store';
import {PersistGate} from 'redux-persist/integration/react'
import { Router } from 'react-router-dom';

class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor} >
                    <Router basename="/" history={history} >
                        <AppContainer  />
                    </Router>
                </PersistGate>
            </Provider>
        );
    }
}

export default App;