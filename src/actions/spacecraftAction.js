export const Types = {
    GET_INTRO: 'GET_INTRO',
    GET_DATA: 'GET_DATA',
    LOADING_DATA: 'LOADING_DATA',
    ORD_DATA: 'ORD_DATA',
    ORD_CALL: 'ORD_CALL',
    YEAR_DATA: 'YEAR_DATA',
    MESSAGE_DATA: 'MESSAGE_DATA',
};


export const getIntroAction = (val) => {
    return {
        type: Types.GET_INTRO,
        id: val
    };
};
export const getMessageAction = (val) => {
    return {
        type: Types.MESSAGE_DATA,
        id: val
    };
};

export const getYearAction = (val) => {
    return {
        type: Types.YEAR_DATA,
        id: val
    };
};


export const getOrdCallAction = (val) => {
    return {
        type: Types.ORD_CALL,
        id: val
    };
};



export const getDataAction = (val) => {
    return {
        type: Types.GET_DATA,
        id: val
    };
};

export const getLoadingAction = (val) => {
    return {
        type: Types.LOADING_DATA,
        id: val
    };
};

export const ordDataAction = (val) => {
    return {
        type: Types.ORD_DATA,
        id: val
    };
};