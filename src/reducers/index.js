import {combineReducers} from 'redux';
import spacecraft from './spacecraftReducer';


export default combineReducers({
    spacecraft: spacecraft
});