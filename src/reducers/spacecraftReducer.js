import {Types} from '../actions/spacecraftAction';

const INITIAL_STATE = {
    data: {},
    loading: false,
    ord: true,
    message: ''
};

export default (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case Types.GET_DATA:
            return {...state, data: action.id};
        case Types.LOADING_DATA:
            return {...state, loading: action.id};
        case Types.ORD_DATA:
            return {...state, ord: action.id};
        case Types.MESSAGE_DATA:
            return {...state, message: action.id};
        default:
            return state;
    }
};


